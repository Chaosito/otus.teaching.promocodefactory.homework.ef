﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DatabaseContext _dbcontext;

        public EfRepository(DatabaseContext context)
        {
            _dbcontext = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbcontext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbcontext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> CreateAsync(T entity)
        {
            var item = await _dbcontext.Set<T>().AddAsync(entity);
            await _dbcontext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(Guid Id)
        {
            var item = await _dbcontext.Set<T>().FirstOrDefaultAsync(x => x.Id == Id);
            _dbcontext.Set<T>().Remove(item);

            await _dbcontext.SaveChangesAsync();
        }

        public async Task<T> UpdateAsync(Guid Id, T entity)
        {
            var item = await _dbcontext.Set<T>().FirstOrDefaultAsync(x => x.Id == Id);
            item = entity;
            await _dbcontext.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _dbcontext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

    }
}
