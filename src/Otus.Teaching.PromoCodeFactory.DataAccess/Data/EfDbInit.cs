﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInit : IDbInit
    {
        private readonly DatabaseContext _dbContext;
        private readonly IConfiguration _configuration;

        public EfDbInit(DatabaseContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public void InitDb()
        {
            bool recreateDb;
            bool fakeDataSeed;
            bool.TryParse(_configuration.GetSection("RecreateDbWhenExists").Value, out recreateDb);
            bool.TryParse(_configuration.GetSection("FakeDataSeed").Value, out fakeDataSeed);

            if (recreateDb)
            {
                _dbContext.Database.EnsureDeleted();
                _dbContext.Database.EnsureCreated();
            }

            if (fakeDataSeed)
            {
                //_dbContext.Database.Migrate();
                _dbContext.AddRange(FakeDataFactory.Roles);
                _dbContext.AddRange(FakeDataFactory.Preferences);
                _dbContext.AddRange(FakeDataFactory.Customers);
                _dbContext.AddRange(FakeDataFactory.Employees);
                _dbContext.SaveChanges();
            }
        }

    }
}
