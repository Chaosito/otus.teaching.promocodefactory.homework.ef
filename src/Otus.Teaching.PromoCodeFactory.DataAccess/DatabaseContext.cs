﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {}

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Employee
            modelBuilder.Entity<Employee>().HasOne(e => e.Role).WithMany(g => g.Employees).HasForeignKey(e => e.RoleId);
            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(50);

            // Role
            modelBuilder.Entity<Role>().HasMany(e => e.Employees).WithOne(e => e.Role).HasForeignKey(e => e.RoleId);
            modelBuilder.Entity<Role>().Property(e => e.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(e => e.Description).HasMaxLength(200);

            // Preference
            modelBuilder.Entity<Preference>().Property(e => e.Name).HasMaxLength(100);

            // Customer
            modelBuilder.Entity<Customer>().HasMany(e => e.PromoCodes).WithOne(e => e.Customer);
            modelBuilder.Entity<Customer>().HasMany(e => e.CustomerPreferences).WithOne(e => e.Customer);
            modelBuilder.Entity<Customer>().Property(e => e.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(e => e.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(e => e.FullName).HasComputedColumnSql("[FirstName] || ' ' || [LastName]");

            // Promocode
            modelBuilder.Entity<PromoCode>().HasOne(e => e.PartnerManager).WithMany().HasForeignKey(e => e.PartnerManagerId);
            modelBuilder.Entity<PromoCode>().HasOne(e => e.Preference).WithMany().HasForeignKey(e => e.PreferenceId);
            modelBuilder.Entity<PromoCode>().Property(e => e.Code).HasMaxLength(15);
            modelBuilder.Entity<PromoCode>().Property(e => e.PartnerName).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(e => e.ServiceInfo).HasMaxLength(50);

            // CustomerPreference
            modelBuilder.Entity<CustomerPreference>().ToTable("CustomerPreference");
            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>().HasOne(c => c.Customer).WithMany(y => y.CustomerPreferences).HasForeignKey(y => y.CustomerId);
            modelBuilder.Entity<CustomerPreference>().HasOne(c => c.Preference).WithMany(y => y.CustomerPreferences).HasForeignKey(y => y.PreferenceId);
        }
    }
}
