﻿using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<CustomerShortResponse> Customers { get; set; }
    }
}
