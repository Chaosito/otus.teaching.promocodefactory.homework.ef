﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(
            IRepository<PromoCode> promocodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Employee> employeeRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            var model = promocodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToShortDateString(),
                EndDate = x.EndDate.ToShortDateString(),
                Code = x.Code,
            });

            if (model.Count() == 0)
            {
                return NoContent();
            }

            return Ok(model);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var employee = (await _employeeRepository.GetAllAsync()).FirstOrDefault(x => x.FullName == request.PartnerName);
            var customers = (await _customerRepository.GetAllAsync()).Where(x => x.CustomerPreferences.Any(y => y.Preference.Name == request.Preference));

            if (employee == null)
            {
                return NotFound("Cотрудник не найден!");
            }

            if (customers.Count() == 0)
            {
                return NotFound("Нет покупателей с такими предпочтениями!");
            }

            foreach (var custom in customers)
            {
                var promo = new PromoCode()
                {
                    PartnerManagerId = employee.Id,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(90),
                    PartnerName = employee.FullName,
                    ServiceInfo = request.ServiceInfo,
                    Code = request.PromoCode,
                    PreferenceId = custom.CustomerPreferences.FirstOrDefault(x => x.Preference.Name == request.Preference).PreferenceId,
                };

                custom.PromoCodes.Add(promo);
                await _customerRepository.UpdateAsync(custom.Id, custom);
            }

            return Ok();
        }
    }
}